import styled from "styled-components";
import Variables from "../../shared/styles/Variables";

export const StyledHeader = styled.nav`
  padding-top: 1rem;
  padding-bottom: 1rem;
  background-color: ${Variables.headerBackgroundColor};
  margin-bottom: 2rem;

  .navbar-item {
    transition: color 0.2s ease-in-out;
  }

  .brand-text {
    font-size: 2em;
  }
`;
