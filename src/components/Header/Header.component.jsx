import React from "react";
import { Link } from "react-router-dom";

import { StyledHeader } from "./Header.styles";

const Header = () => {
  return (
    <StyledHeader
      className="navbar"
      role="navigation"
      aria-label="main navigation"
    >
      <div className="container">
        <div className="navbar-brand">
          <Link to="/" className="navbar-item">
            <p className="brand-text">QS React App</p>
          </Link>
        </div>
      </div>
    </StyledHeader>
  );
};

export default Header;
