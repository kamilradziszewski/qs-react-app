import React, { useEffect, useState } from "react";

import PlaceholderImage from "../../../assets/images/placeholder-image.png";

const ItemImage = ({ images }) => {
  const [mainImage, setMainImage] = useState({
    url: PlaceholderImage,
    altText: "",
  });

  useEffect(() => {
    if (images && images.length > 0) {
      setMainImage({
        url: `${images[0].image.asset.url}?w=320&h=320`,
        altText: images[0].alt,
      });
    }
  }, [images]);

  return (
    <img
      src={mainImage.url}
      alt={mainImage.altText}
      className="item-image"
    />
  );
};

export default ItemImage;
