import React from "react";

import { StyledItemArticlesList } from "./ItemArticlesList.styles";

const ItemArticlesList = ({ articlesList }) => {
  if (!articlesList) return null;

  return (
    <StyledItemArticlesList>
      <p className="subtitle">W tym numerze:</p>
      <ul>
        {articlesList.map((info, index) => (
          <li key={index} className="item-article">
            &ndash; {info}
          </li>
        ))}
      </ul>
    </StyledItemArticlesList>
  );
};

export default ItemArticlesList;
