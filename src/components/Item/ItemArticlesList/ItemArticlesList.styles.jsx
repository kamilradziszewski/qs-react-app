import styled from "styled-components";

export const StyledItemArticlesList = styled.div`
  margin-bottom: 2rem;

  .item-article {
    margin-bottom: 0.2em;
  }
`;
