import React from "react";

import { StyledItemExtraInfo } from "./ItemExtraInfo.styles";

const ItemExtraInfo = ({ extraInfoContent }) => {
  if (extraInfoContent.length === 0) return null;

  return (
    <StyledItemExtraInfo className="box">
      {extraInfoContent.map((info, index) => (
        <li key={index} className="item-extra-info">
          {info}
        </li>
      ))}
    </StyledItemExtraInfo>
  );
};

export default ItemExtraInfo;
