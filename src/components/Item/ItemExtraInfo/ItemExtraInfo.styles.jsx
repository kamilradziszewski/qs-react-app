import styled from "styled-components";

export const StyledItemExtraInfo = styled.ul`
  background-color: #fcfcfc;
  padding: 1rem;
  margin-bottom: 3rem !important;
`;
