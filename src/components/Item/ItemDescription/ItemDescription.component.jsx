import React from "react";

import { StyledItemDescription } from "./ItemDescription.styles";

const ItemDescription = ({ description }) => {
  return <StyledItemDescription blocks={description} />;
};

export default ItemDescription;
