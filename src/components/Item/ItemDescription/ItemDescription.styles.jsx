import styled from "styled-components";
import BlockContent from "@sanity/block-content-to-react";

export const StyledItemDescription = styled(BlockContent)`
  margin-bottom: 2rem;
`;
