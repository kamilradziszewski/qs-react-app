import React from "react";

const ItemImageGallery = ({ images }) => {
  const imageGallery =
    images && images.length > 1
      ? images.map((image, index) => {
          return index !== 0 ? (
            <div
              className="column is-narrow"
              key={image.image.asset.id}
            >
              <img
                src={`${image.image.asset.url}?h=240`}
                alt=""
              />
            </div>
          ) : null;
        })
      : null;

  return imageGallery ? (
    <div className="columns is-mobile is-multiline item-image-gallery">
      {imageGallery}
    </div>
  ) : null;
};

export default ItemImageGallery;
