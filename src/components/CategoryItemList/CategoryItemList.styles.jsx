import styled from "styled-components";

export const StyledCategoryItemList = styled.div`
  margin-bottom: 2rem;

  h2 {
    font-size: 1.7em;
    margin-bottom: 0.5em;
    color: ${({ color }) => color};
  }
`;
