import React from "react";

import CategoryItem from "../CategoryItem/CategoryItem.component";

import { StyledCategoryItemList } from "./CategoryItemList.styles";

const CategoryItemList = ({
  title,
  category,
  color,
  items,
}) => {
  return (
    <StyledCategoryItemList color={color}>
      <h2>{title}</h2>
      <div className="columns is-multiline">
        {items.map((item) => {
          return (
            <div
              className="column is-half-tablet is-one-quarter-desktop"
              key={item.id}
            >
              <CategoryItem
                {...item}
                category={category}
                color={color}
              />
            </div>
          );
        })}
      </div>
    </StyledCategoryItemList>
  );
};

export default CategoryItemList;
