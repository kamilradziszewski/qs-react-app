import React from "react";
import { Link } from "react-router-dom";

import { StyledCard } from "./CategoryItem.styles";

const CategoryItem = ({
  id,
  slug,
  title,
  author,
  category,
  color,
}) => (
  <Link to={`/${category}/${slug ? slug.current : ""}`}>
    <StyledCard color={color} className="card">
      <header className="card-header">
        <h3 className="card-header-title">{title}</h3>
      </header>
      <div className="card-content">
        <p>{author}</p>
      </div>
    </StyledCard>
  </Link>
);

export default CategoryItem;
