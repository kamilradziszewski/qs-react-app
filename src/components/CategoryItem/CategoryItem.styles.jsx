import styled, { css } from "styled-components";
import { lighten } from "polished";

export const StyledCard = styled.div`
  transition: color 0.2s ease-in-out;

  .card-header-title {
    font-weight: 500;
    font-size: 1.1em;
  }

  &:hover {
    .card-header-title {
      color: ${({ color }) => color};
    }
  }

  ${({ color }) =>
    color &&
    css`
      /* background-color: ${lighten(0.55, color)}; */
    `}
  height: 100%;
`;
