import React from "react";

import Header from "../Header/Header.component";
import Footer from "../Footer/Footer.component";

import { StyledLayout } from "./Layout.styles";

const Layout = ({ children }) => {
  return (
    <StyledLayout>
      <Header />
      <main>
        <div className="container">{children}</div>
      </main>
      <Footer />
    </StyledLayout>
  );
};

export default Layout;
