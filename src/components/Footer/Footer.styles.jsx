import styled from "styled-components";
import Variables from "../../shared/styles/Variables";

export const StyledFooter = styled.footer`
  padding-top: 1.2rem;
  padding-bottom: 1.2rem;
  background-color: ${Variables.headerBackgroundColor};
  margin-top: 2rem;
  text-align: center;
  font-size: 0.8rem;
`;
