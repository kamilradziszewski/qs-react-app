import React from "react";

import { StyledFooter } from "./Footer.styles";

const Footer = () => {
  return (
    <StyledFooter>
      <div className="container is-fluid">
        &copy; {new Date().getFullYear()} QS
      </div>
    </StyledFooter>
  );
};

export default Footer;
