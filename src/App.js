import React from "react";
import { Route } from "react-router-dom";

import "./App.scss";

import Layout from "./components/Layout/Layout.component";

import Homepage from "./pages/Homepage";
import Book from "./pages/Book";
import Magazine from "./pages/Magazine";

const App = () => {
  return (
    <Layout>
      <Route path="/" exact component={Homepage} />
      <Route path="/book/:slug" component={Book} />
      <Route path="/magazine/:slug" component={Magazine} />
    </Layout>
  );
};

export default App;
