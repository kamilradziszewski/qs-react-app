const items = {
  booksList: {
    title: "Książki",
    category: "book",
    color: "#7017a8",
    items: [
      {
        id: "B07YKTYZJ6",
        title: "Legacy of Lies: A Legal Thriller",
        extraInfo: "Robert Bailey",
        image:
          "https://www.bookcoversclub.com/wp-content/uploads/2018/02/book-cover-352.jpg",
      },
      {
        id: "B07W7BT2BL",
        title: "Sorry I Missed You: A Novel",
        extraInfo: "Suzy Krause",
        image: "https://www.bookcoversclub.com/wp-content/uploads/2017/07/book-cover-338.jpg"
      },
      {
        id: "B07CLNDCFH",
        title: "The Bad Seed",
        extraInfo: "Jory John",
      },
      {
        id: "B07VXFFKMQ",
        title: "If You Must Know: A Novel",
        extraInfo: "Jamie Beck",
        image: "https://www.bookcoversclub.com/wp-content/uploads/2018/07/book-cover-354.jpg"
      },
    ],
  },
  magazinesList: {
    title: "Magazyny",
    category: "magazine",
    color: "#0039b5",
    items: [
      {
        id: "389584",
        title: "Frictie",
        extraInfo: "",
      },
      {
        id: "285930",
        title: "Critique and Humanism",
        extraInfo: "",
      },
      {
        id: "265738",
        title: "Dialogi",
        extraInfo: "",
      },
    ],
  },
  flyersList: {
    title: "Ulotki",
    category: "flyer",
    color: "#2c4e00",
    items: [
      {
        id: "17482",
        title: "Profilowanie kryminalne",
        extraInfo: "Worek Kości",
      },
      {
        id: "60600",
        title:
          "Warsztaty dla muzyków: Event Management & Media Relations",
        extraInfo: "",
      },
      {
        id: "28693",
        title:
          "Wojciech Bąkowski: Analiza wzruszeń i rozdrażnień 2",
        extraInfo: "",
      },
    ],
  },
};

export default items;
