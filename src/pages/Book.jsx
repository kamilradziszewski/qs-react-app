import React from "react";
import { useParams } from "react-router-dom";

import ItemDescription from "../components/Item/ItemDescription/ItemDescription.component";
import ItemExtraInfo from "../components/Item/ItemExtraInfo/ItemExtraInfo.component";
import ItemImage from "../components/Item/ItemImage/ItemImage.component";
import ItemImageGallery from "../components/Item/ItemImageGallery/ItemImageGallery.component";

import { StyledBook } from "./Book.styles";

import { Query } from "react-apollo";
import { gql } from "apollo-boost";

const BOOK = gql`
  query getBook($slug: String!) {
    currentBook: allBook(
      where: { slug: { current: { eq: $slug } } }
    ) {
      id: _id
      slug {
        current
      }
      title
      author
      descriptionRaw
      datePublished
      isbn
      numOfPages
      images {
        image {
          asset {
            id: _id
            url
            originalFilename
          }
        }
        alt
      }
    }
  }
`;

const Book = () => {
  const { slug } = useParams();

  const getExtraInfoContentArray = (book) => {
    const extraInfoContent = [];
    extraInfoContent.push(
      book.numOfPages
        ? `Ilość stron: ${book.numOfPages}`
        : null
    );
    extraInfoContent.push(
      book.datePublished
        ? `Data wydania: ${book.datePublished}`
        : null
    );
    extraInfoContent.push(
      book.isbn ? `ISBN: ${book.isbn}` : null
    );

    return extraInfoContent;
  };

  return (
    <StyledBook>
      <Query
        query={BOOK}
        variables={{
          slug,
        }}
      >
        {({ data, loading, error }) => {
          if (loading) return <p>Loading…</p>;
          if (error) return <p>Something went wrong</p>;

          const book = data.currentBook[0];

          return (
            <>
              <h1 className="item-title">{book.title}</h1>
              <div className="columns">
                <div className="column is-one-third-tablet is-one-quarter-desktop">
                  <div className="image-wrapper">
                    <ItemImage images={book.images} />
                  </div>
                </div>

                <div className="column is-two-thirds-tablet is-three-quarters-desktop">
                  <div className="text-wrapper">
                    <h2 className="item-subtitle">
                      {book.author}
                    </h2>
                    <ItemDescription
                      description={book.descriptionRaw}
                    />
                    <ItemExtraInfo
                      extraInfoContent={getExtraInfoContentArray(
                        book
                      )}
                    />
                    <ItemImageGallery
                      images={book.images}
                    />
                  </div>
                </div>
              </div>
            </>
          );
        }}
      </Query>
    </StyledBook>
  );
};

export default Book;
