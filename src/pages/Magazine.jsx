import React from "react";
import { useParams } from "react-router-dom";

// import ItemDescription from "../components/Item/ItemDescription/ItemDescription.component";
import ItemArticlesList from "../components/Item/ItemArticlesList/ItemArticlesList.component";
import ItemExtraInfo from "../components/Item/ItemExtraInfo/ItemExtraInfo.component";
import ItemImage from "../components/Item/ItemImage/ItemImage.component";
import ItemImageGallery from "../components/Item/ItemImageGallery/ItemImageGallery.component";

// import { StyledBook } from "./Book.styles";

import { Query } from "react-apollo";
import { gql } from "apollo-boost";

const MAGAZINE = gql`
  query getMagazine($slug: String!) {
    currentMagazine: allMagazine(
      where: { slug: { current: { eq: $slug } } }
    ) {
      id: _id
      slug {
        current
      }
      title
      issueNumber
      datePublished
      period
      articles
      images {
        image {
          asset {
            id: _id
            url
            originalFilename
          }
        }
        alt
      }
    }
  }
`;

const Magazine = () => {
  const { slug } = useParams();

  const getExtraInfoContentArray = (magazine) => {
    const extraInfoContent = [];
    extraInfoContent.push(
      magazine.datePublished
        ? `Data wydania: ${magazine.datePublished}`
        : null
    );
    extraInfoContent.push(
      magazine.period
        ? `Częstotliwość wydania: ${magazine.period}`
        : null
    );

    return extraInfoContent;
  };

  return (
    <div>
      <Query
        query={MAGAZINE}
        variables={{
          slug,
        }}
      >
        {({ data, loading, error }) => {
          if (loading) return <p>Loading…</p>;
          if (error) return <p>Something went wrong</p>;

          const magazine = data.currentMagazine[0];

          return (
            <>
              <h1 className="item-title title">
                {magazine.title} {magazine.issueNumber}
              </h1>
              <div className="columns">
                <div className="column is-one-third-tablet is-one-quarter-desktop">
                  <div className="image-wrapper">
                    <ItemImage images={magazine.images} />
                  </div>
                </div>

                <div className="column is-two-thirds-tablet is-three-quarters-desktop">
                  <div className="text-wrapper">
                    <ItemArticlesList
                      articlesList={
                        magazine.articles
                          ? magazine.articles
                          : null
                      }
                    />

                    <ItemExtraInfo
                      extraInfoContent={getExtraInfoContentArray(
                        magazine
                      )}
                    />
                    <ItemImageGallery
                      images={magazine.images}
                    />
                  </div>
                </div>
              </div>
            </>
          );
        }}
      </Query>
    </div>
  );
};

export default Magazine;
