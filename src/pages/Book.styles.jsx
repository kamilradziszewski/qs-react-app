import styled from "styled-components";

export const StyledBook = styled.div`
  .item-title {
    text-transform: uppercase;
    font-size: 1.75em;
    margin-bottom: 1.5em;
  }

  .image-wrapper {
    display: flex;
    justify-content: center;
  }

  .text-wrapper {
  }

  .item-subtitle {
    text-transform: uppercase;
    font-size: 1.25em;
    margin-bottom: 1em;
    letter-spacing: 0.02em;
  }
`;
