import React from "react";

import CategoryItemList from "../components/CategoryItemList/CategoryItemList.component";

import items from "../shared/data/items";

import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

const ALL_ITEMS = gql`
  {
    allBook {
      id: _id
      slug {
        current
      }
      title
      author
      descriptionRaw
      datePublished
      isbn
      numOfPages
      images {
        image {
          asset {
            url
          }
        }
        alt
      }
    }
    allMagazine {
      id: _id
      slug {
        current
      }
      title
      issueNumber
      datePublished
      period
      articles
      images {
        image {
          asset {
            url
          }
        }
        alt
      }
    }
  }
`;

const Homepage = () => {
  const { loading, error, data } = useQuery(ALL_ITEMS);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  const booksData = {
    title: "Książki",
    category: "book",
    color: "#7017a8",
    items: [...data.allBook],
  };

  const magazinesData = {
    title: "Magazines",
    category: "magazine",
    color: "#0039b5",
    items: [...data.allMagazine],
  };

  return (
    <>
      <CategoryItemList {...booksData} />
      <CategoryItemList {...magazinesData} />
      <CategoryItemList {...items.flyersList} />
    </>
  );
};

export default Homepage;
