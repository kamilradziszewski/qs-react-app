import ApolloClient from "apollo-boost";

const client = new ApolloClient({
  uri:
    "https://smptdkq4.api.sanity.io/v1/graphql/production/default",
});

export default client;
